import json


def print_dictionary_contents():
    english_dictionary = json.load(open('simple_english_dictionary.json'))

    count = 0
    for word, definition in english_dictionary.items():
        print(word)
        print(definition)
        print()
        count += 1

    print('Total words:', count)


if __name__ == '__main__':
    print_dictionary_contents()
